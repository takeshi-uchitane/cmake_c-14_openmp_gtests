#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Bar.hpp"

TEST(Bar, constractor) {
  Bar bar;
  EXPECT_THAT(bar.v, testing::ElementsAre(0,1,2,3));
  EXPECT_THAT(bar.calc(), testing::ElementsAre(3.0,2.0,1.0,0.0));
}

