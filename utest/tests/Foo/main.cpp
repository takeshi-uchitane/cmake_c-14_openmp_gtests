#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Foo.hpp"

TEST(Foo, constractor) {
  Foo foo;
  EXPECT_THAT(foo.v, testing::ElementsAre(0,1,2,3));
  EXPECT_THAT(foo.calc(), testing::ElementsAre(0.0,1.0,2.0,3.0));
}

