cmake_minimum_required(VERSION 2.8)

add_executable(foo-test main.cpp ${SOURCES})
target_link_libraries(foo-test
  gtest
  gtest_main
  gmock
  gmock_main
  pthread
)
add_test(
  NAME foo
  COMMAND $<TARGET_FILE:foo-test>
  )

# run with: ctest -L lib
set_property(
  TEST foo
  PROPERTY LABELS lib foo
  )

