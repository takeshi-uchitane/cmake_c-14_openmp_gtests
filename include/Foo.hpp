#ifndef FOO_HPP_INCLUDED
#define FOO_HPP_INCLUDED

#include <vector>
#include <algorithm>

class Foo
{
  public:
    Foo();
    std::vector<double> calc();
    std::vector<unsigned int> v;
};

#endif // FOO_HPP_INCLUDED

