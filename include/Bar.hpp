#ifndef BAR_HPP_INCLUDED
#define BAR_HPP_INCLUDED

#include <vector>
#include <algorithm>

class Bar
{
  public:
    Bar();
    std::vector<double> calc();
    std::vector<unsigned int> v;
};

#endif // BAR_HPP_INCLUDED

