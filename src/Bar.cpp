#include "Bar.hpp"

Bar::Bar() { for(unsigned int i=0u;i<4u;i++) { v.push_back(i); } }
std::vector<double> Bar::calc() {
  std::vector<double> array;
  for(auto e : v) { array.push_back(double(e)); }
  sort(array.begin(), array.end(), [](auto right, auto left){ return right > left; });
  return std::move(array);
}

