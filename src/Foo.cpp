#include "Foo.hpp"
#include <omp.h>
#include <iostream>

Foo::Foo() { for(unsigned int i=0u;i<4u;i++) { v.push_back(i); } }

std::vector<double> Foo::calc() {
  std::vector<double> array(v.size());
#pragma omp parallel for
  for(unsigned int i=0u;i<v.size();i++) {
    array[i]=double(v[i]);
    std::cout << "thread " << omp_get_thread_num() << std::endl;
  }
  sort(array.begin(), array.end());
  return std::move(array);
}

